Automatic Tool Changer (ATC)
============================

 This macro was created for a simple ATC with a fixed five slots tool magazine.
 Adjust the parameters in the `init.ngc` file to your machine environment!

 This script is free software, distributed under the terms of the GPL3+.
 This script comes without any warranty!


## TODO

 - Implement Tool numbers from six and higher for manual tool changing
   - Move Spindle/Tool near the reference point


## ATC Macro Files

 - `macros/atc.ngc`
 - `macros/atc/init.ngc`
 - `macros/atc/select_slot.ngc`
 - `macros/atc/pick_up.ngc`
 - `macros/atc/place_down.ngc`
 - `macros/atc/manual_change.ngc`
 - `macros/atc/README.md`


## Usage

 - The ATC itself works with Tools from `T1` to `T5`.
   - The machine picks and places tools in the ATC magazine slots.
 - `T0` means empty ATC.
 - `Tools from `T6` and higher are used for manual changing.
   - The machine moves to a place near the reference point and waits for a user action.
   - The machine executes no pick up and place down procedures.


## LinuxCNC HAL Configuration

 The ATC is connected to `motion.digital-out-00`
 and callable with `M62 - M65` __Digital Output Control__.

 ``` custom.hal
 setp parport.0.pin-14-out FALSE
 net atc parport.0.pin-14-out <= motion.digital-out-00 
 ```


 The `M6` __Tool Change__ command is remaped to the ATC macro.

 ``` my-machine.ini
 [RS274NGC]
 REMAP= M6 modalgroup=6 ngc=macros/atc
 ```


## References

### Hardware

 - Machine: TODO...
 - Spindle: TODO...
 - ATC:     TODO...


### LinuxCNC Documentation

 - <http://linuxcnc.org/docs/devel/html/gcode/m-code.html#_tool_changer>
 - <http://linuxcnc.org/docs/devel/html/gcode/o-code.html#ocode:calling-files>
 - <http://linuxcnc.org/docs/devel/html/gcode/overview.html#gcode:parameters>
 - <http://linuxcnc.org/docs/devel/html/gcode/overview.html#gcode:predefined-named-parameters>
