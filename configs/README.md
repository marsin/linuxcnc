Linux CNC
=========

 LinuxCNC configuration for Stepcraft2-D.600 milling machine.
 This configuration comes without any warranty.


Usage
-----

 - Press machine E-Stop first, then start LinuxCNC!
   - The ATC collet opens, when starting LinuxCNC (and E-Stop is not pressed)
 - Add `4000 0.000000` to your `.var` files!
   This is the value of the current tool in the tool changer
 - `M6` is remapped to the ATC macro, M31 is mapped to the TLS
   - `M6 T1 M31 G43`: Get Tool1, get tool length, enable tool offset


Parallel Port 1
---------------

 - IO:  378h
 - IRQ: 5


 | Pin | I/O | Controller Signal  | Machine Signal        | Annotation      |
 | --: | :-- | :----------------- | :-------------------- | :-------------- |
 |   1 | out | Spindle ON         | Relais 1              |                 |
 |   2 | out | X Direction        | Dir X                 |                 |
 |   3 | out | X Step             | Clock X               |                 |
 |   4 | out | Y Direction        | Dir Y                 |                 |
 |   5 | out | Y Step             | Clock Y               |                 |
 |   6 | out | Z Direction        | Dir Z                 |                 |
 |   7 | out | Z Step             | Clock Z               |                 |
 |   8 | out | A Direction        | Dir 4th Axis          | _not connected_ |
 |   9 | out | A Step             | Clock 4th Axis        | _not connected_ |
 |  10 |  in | Probe In           | Tool Length Sensor    |                 |
 |  11 |  in | ESTOP In           | Error (E-Stop)        | invert          |
 |  12 |  in | All limits + homes | Limit Switch X/Y/Z    |                 |
 |  13 |  in | _<unused>_         | Limit Switch 4th Axis |                 |
 |  14 | out | Digital out 0      | Relais 1 (ATC collet) |                 |
 |  15 |  in | _<unused>_         | Housing (I 15)        |                 |
 |  16 | out | Digital out 1      | Relais 3              |                 |
 |  17 | out | Spindle PWM        | PWM                   |                 |
 |  18 | GND |                    |                       |                 |
 |  19 | GND |                    |                       |                 |
 |  20 | GND |                    |                       |                 |
 |  21 | GND |                    |                       |                 |
 |  22 | GND |                    |                       |                 |
 |  23 | GND |                    |                       |                 |
 |  24 | GND |                    |                       |                 |
 |  25 | GND |                    |                       |                 |


Machine Hardware
----------------

### Stepcraft2-D.600

 - Manual
   - Stepper 
     - 1.8 deg/full_step
     - Current max.:
       - X Axis: 1.4 A
       - Y Axis: 1.8 A
       - Z Axis: 1.4 A

   - Joint
     - 3 mm/rotation

   - Other
     - Backlash:   0.08 mm
     - Speed max.: 3000 mm/min
 

### Spindle HF500

 - Manual: Spindle HF350 / HF500
   - Rotation Speed Range: 3000-20000 rpm, clockwise
   - Voltage max.: 24    V
   - Current max.: 20.8  A
   - Torque max.:  0.25 Nm

 - Links
   - <https://shop.stepcraft-systems.com/hf-spindle-500-w>
   - <https://www.stepcraft-systems.com/en/forum/software2/4753-uccnc-pwm-hf-500>


### Automatic Tool Changer (ATC)

 - Manual: Toolchanger for HF350 / HF500
   - Operating Presure: 8-10 bar
   - Rotation max.:     20000 rpm

 - Links
   - <https://shop.stepcraft-systems.com/automatic-tool-changer-hf-spindle>


